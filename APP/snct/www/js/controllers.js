angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {} )

.controller('HomeCtrl', function( $scope, $state) {
	$scope.openPage = function( $page ) {
	    $state.go( $page );
	};
} )

.controller('ProgramacaoCtrl', function( $scope, $state, $http) {
    $http.get('http://www.sintonizeciencia.com.br/wp-json/taxonomies/state/terms/?filter[parent]=0')
    .success( function(data) {
        $scope.terms = data;
    })
} )

.controller('CidadeCtrl', function( $scope, $state, $stateParams, $http) {
    $scope.estado = $stateParams.estadoId;
    $http.get('http://www.sintonizeciencia.com.br/wp-json/taxonomies/state/terms/?filter[parent]=' + $stateParams.estadoId)
    .success( function(data) {
        $scope.terms = data;
    })
} )

.controller('EventosCtrl', function( $scope, $state, $http, $stateParams) {
    $scope.estado = $stateParams.estadoId;
    $scope.cidade = $stateParams.cidadeId;
    $http.get('http://sintonizeciencia.com.br/wp-json/posts?type=schedule&filter[posts_per_page]=-1&filter[taxonomy]=state&filter[term]=' + $stateParams.cidadeId)
    .success( function(data) {
        $scope.posts = data;
    })
} )

.controller('EventoCtrl', function( $scope, $state, $ionicLoading, $compile, $http, $stateParams ) {
    var post_id = $stateParams.eventoId;
    $scope.ppost_id = $stateParams.eventoId;
    $scope.init = function() {
        $http.get('http://sintonizeciencia.com.br/wp-json/posts?type=schedule&filter[p]=' + post_id)
        .success( function(data) {
            $scope.posts = data;
            $scope.latitude = data[0].meta.latitude;
            $scope.longitude = data[0].meta.longitude;
            var myLatlng = new google.maps.LatLng($scope.latitude,$scope.longitude);
            var mapOptions = {
                center: myLatlng,
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map( document.getElementById("map"), mapOptions );
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Titulo'
            });
            $scope.map = map;
        });
    }

    $scope.comoChegar = function(latitude, longitude) {
        var onSuccess = function(position) {
            // var directionsDisplay;
            // var directionsService = new google.maps.DirectionsService();
            // var map;

            // directionsDisplay = new google.maps.DirectionsRenderer();
            // var myLatlng = new google.maps.LatLng(-15.7675587,-47.8936927);
            // var mapOptions = {
            //     zoom:12,
            //     center: myLatlng
            // }
            // map = new google.maps.Map(document.getElementById("map"), mapOptions);
            // directionsDisplay.setMap(map);

            // var local_origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            // var local_destino = new google.maps.LatLng(-15.7675587,-47.8936927);
            // var request = {
            //     origin:local_origin,
            //     destination:local_destino,
            //     travelMode: google.maps.TravelMode.DRIVING
            // };
            // directionsService.route(request, function(result, status) {
            //     if (status == google.maps.DirectionsStatus.OK) {
            //         directionsDisplay.setDirections(result);
            //     }
            // });

            var url = 'https://www.google.com.br/maps/dir/' + position.coords.latitude + ',' + position.coords.longitude + '/' + latitude + ',' + longitude;
            var ref = window.open(url, '_system', 'location=no');
        };

        function onError(error) {
            alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
        }
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }

    $scope.addAgenda = function(post_id) {
        var agenda_atual = window.localStorage['agenda_id_post'];
        if( agenda_atual === "" ){
            agenda_atual = post_id;
        }
        else {
            agenda_atual = agenda_atual + ',' + post_id;
        }
        window.localStorage['agenda_id_post'] = agenda_atual;
        alert('Evento adicionado a sua agenda.');
    }
} )

.controller('LocalizacoesCtrl', function( $scope, $state, $ionicLoading, $compile, $http ) {
    $scope.init = function() {
        var onSuccess = function(position) {
            var markersData = [];
            $http.get('http://sintonizeciencia.com.br/wp-json/posts?type=schedule&filter[posts_per_page]=-1')
            .success( function(data) {
                $scope.posts = data;
                posts = $scope.posts;
                for (var i = 0; i < $scope.posts.length; i++) {
                    markersData.push({
                        'lat' : $scope.posts[i].meta.latitude,
                        'lng' : $scope.posts[i].meta.longitude,
                        'nome' : $scope.posts[i].title,
                        'morada1' : $scope.posts[i].meta.endereco,
                    });
                }

                markersData.push({
                    'lat' : position.coords.latitude,
                    'lng' : position.coords.longitude,
                    'nome' : "Você está aqui!",
                    'morada1' : ""
                });

                var myLatlng = new google.maps.LatLng(-15.7675587,-47.8936927);
                var mapOptions = {
                    center: myLatlng,
                    zoom: 6,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map( document.getElementById("mapa_localizacao"), mapOptions );
                var infowindow = new google.maps.InfoWindow();
                var bounds = new google.maps.LatLngBounds();

                // google.maps.event.addListener(map, 'click', function() {
                //     infoWindow.close();
                // });

                for (var i = 0; i < markersData.length; i++) {
                    var p = markersData[i];
                    var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
                    bounds.extend(latlng);

                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        titulo: markersData[i].nome,
                        endereco: markersData[i].morada1,
                    });

                    google.maps.event.addListener(marker, 'click', function(){
                        var content = "<div id='infowindow'><h3>" + this.titulo + "</h3><p>" + this.endereco + "</p></div>";
                        infowindow.close();
                        infowindow.setContent( content );
                        infowindow.open(map, this);
                    });
                }
                map.fitBounds(bounds);

                $scope.map = map;
            });
        };
        function onError(error) {
            alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
        }
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }
} )

.controller('FotosCtrl', function( $scope, $state, $http, $cordovaFileTransfer ) {

    $http.get('http://www.sintonizeciencia.com.br/wp-json/posts?type=wall&filter[posts_per_page]=-1')
    .success( function(data) {
        $scope.posts = data;
    })

    $scope.pegar_foto = true;
    $scope.opcao_foto = false;

    $scope.isVisible = function( value ){
        if( value == 1 )
            $scope.pegar_foto = true;
        else
            $scope.pegar_foto = false;
    }

    $scope.mudar_opacao_foto = function( value ) {
         if( value == 1 )
            $scope.opcao_foto = true;
        else
            $scope.opcao_foto = false;
    }

    $scope.openPage = function( $page ) {
        $state.go( $page );
    };

    $scope.opcao_photo = function() {
        $scope.mudar_opacao_foto(1);
    }

    $scope.getPhoto = function( type ) {
        $scope.isVisible(0);
        $scope.mudar_opacao_foto(0);

        if( type == 1 ) {
            var opt = {
                quality: 50,
                // destinationType: Camera.DestinationType.DATA_URL,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA
            };
        }
        if( type == 2) {
            var opt = {
                quality: 50,
                // destinationType: Camera.DestinationType.DATA_URL,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY
            };
        }

        navigator.camera.getPicture(onSuccess, onFail, opt );

        function onSuccess(imageData) {
            $('#image-preview').attr('src', imageData);
            // $('#image-preview').attr('src', "data:image/jpeg;base64,"+imageData);
            // $scope.lastPhoto = dataURItoBlob("data:image/jpeg;base64,"+imageData);

            $scope.picData = imageData;
            if( type == 2 ){
                window.resolveLocalFileSystemURI(imageData, function(fileEntry) {
                    $scope.picData = fileEntry.nativeURL;
                });
            }
        }
        function onFail(message) {
            alert('Failed because: ' + message);
            $scope.isVisible(1);
        }
    }

    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++)  {
            ia[i] = byteString.charCodeAt(i);
        }

        var bb = new Blob([ab], { "type": mimeString });
        return bb;
    }

    $scope.sendPhoto = function(){
        var fileURL = $scope.picData;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.mimeType = "image/jpeg";
        options.chunkedMode = true;

        var params = {};
        params.nome =  $(".onde_estou").val();
        params.onde = $(".meu_nome").val();
        params.comentario = $(".meu_comentario").val();

        options.params = params;

        $cordovaFileTransfer.upload( encodeURI( "http://sintonizeciencia.com.br/upload/" ), fileURL, options)
          .then(function(result) {
            console.log(result);
          }, function(err) {
            console.log(err);
          }, function (progress) {
            console.log("enviando");
        });

        alert("Sua foto foi enviada e está aguardando moderação.");
        $scope.isVisible(1);

        $('#image-preview').attr('src', "");
        $(".onde_estou").val("");
        $(".meu_nome").val("");
        $(".meu_nome").val("");
    }
} )

.controller('FotoCtrl', function( $scope, $state, $stateParams, $http ) {
    $http.get('http://www.sintonizeciencia.com.br/wp-json/posts?type=wall&filter[posts_per_page]=-1')
    .success( function(data) {
        $scope.posts = data;
        $scope.post = $scope.getPost( $stateParams.fotoId );
    });

    $scope.getPost = function( postId ) {
        for (var i = 0; i < $scope.posts.length; i++) {
            if ($scope.posts[i].ID === parseInt(postId)) {
                return $scope.posts[i];
            }
        }
        return null;
    }
} )

.controller('AgendaCtrl', function( $scope, $state, $http ) {
    var post_ids = window.localStorage['agenda_id_post'];
    var filtro = '';
    if( typeof post_ids !== 'undefined' ){
        var post_ids_arrs = post_ids.split(",");
        for (var i in post_ids_arrs) {
            if( typeof post_ids_arrs !== 'undefined'){
                filtro += '&filter[post__in][]=' + post_ids_arrs[i];
            }
        }
        $http.get('http://www.sintonizeciencia.com.br/wp-json/posts?type=schedule' + filtro)
        .success( function(data) {
            $scope.posts = data;
            for( i = 0; i < $scope.posts.length; i++ ) {
                for ( x = 0; x < $scope.posts[i].terms.state.length; x++ ) {
                    if( $scope.posts[i].terms.state[x].parent ){
                        $scope.estado = $scope.posts[i].terms.state[x].ID;
                    } else {
                        $scope.cidade = $scope.posts[i].terms.state[x].slug;
                    }
                }
            }
        })
    }
} )

.controller('AgendaDiaCtrl', function( $scope, $state ) {} )

.controller('NoticiasCtrl', function( $scope, $state, $http ) {
    $http.get('http://www.sintonizeciencia.com.br/wp-json/posts?type=news&filter[posts_per_page]=-1')
    .success( function(data) {
        $scope.posts = data;
    })
} )

.controller('NoticiaCtrl', function( $scope, $state, $stateParams, $http ) {
    $http.get('http://www.sintonizeciencia.com.br/wp-json/posts?type=news&filter[posts_per_page]=-1')
    .success( function(data) {
        $scope.posts = data;
        $scope.post = $scope.getPost( $stateParams.noticiaId );
    });

    $scope.getPost = function( postId ) {
        for (var i = 0; i < $scope.posts.length; i++) {
            if ($scope.posts[i].ID === parseInt(postId)) {
                return $scope.posts[i];
            }
        }
        return null;
    }
} )
