// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

        AppRate.preferences.storeAppURL.ios = '1042477442';
        AppRate.preferences.storeAppURL.android = 'market://details?id=com.opiniao.semanact';
        AppRate.preferences.openStoreInApp = true;
        AppRate.preferences.displayAppName = 'SNCT 2015';
        AppRate.preferences.usesUntilPrompt = 3;
        AppRate.preferences.promptAgainForEachNewVersion = false;
        AppRate.preferences.useLanguage = 'pt';

        // var customLocale = {};
        // customLocale.title = "Rate %@";
        // customLocale.message = "If you enjoy using %@, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!";
        // customLocale.cancelButtonLabel = "No, Thanks";
        // customLocale.laterButtonLabel = "Remind Me Later";
        // customLocale.rateButtonLabel = "Rate It Now";

        // AppRate.preferences.customLocale = customLocale;

        // AppRate.promptForRating(3);

    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    // Slide de abertura
    .state('home', {
        url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
    })

    .state('app.programacao', {
        url: '/programacao',
        views: {
            'menuContent': {
                templateUrl: 'templates/programacao.html',
                controller: 'ProgramacaoCtrl'
            }
        }
    })

    .state('app.cidade', {
        url: '/programacao/:estadoId',
        views: {
            'menuContent':{
                templateUrl: 'templates/cidade.html',
                controller: 'CidadeCtrl'
            }
        }
    })

    .state('app.eventos', {
        url: '/programacao/:estadoID/:cidadeId',
        views: {
            'menuContent':{
                templateUrl: 'templates/eventos.html',
                controller: 'EventosCtrl'
            }
        }
    })

    .state('app.evento', {
        url: '/programacao/:estado/:cidadeId/:eventoId',
        views: {
            'menuContent':{
                templateUrl: 'templates/evento.html',
                controller: 'EventoCtrl'
            }
        }
    })

    .state('app.localizacoes', {
        url: '/localizacoes',
        views: {
            'menuContent': {
                templateUrl: 'templates/localizacoes.html',
                controller: 'LocalizacoesCtrl'
            }
        }
    })

    .state('app.fotos', {
        url: '/fotos',
        views: {
            'menuContent': {
                templateUrl: 'templates/fotos.html',
                controller: 'FotosCtrl'
            }
        }
    })

    .state('app.foto', {
        url: '/fotos/:fotoId',
        views: {
            'menuContent': {
                templateUrl: 'templates/foto.html',
                controller: 'FotoCtrl'
            }
        }
    })

    .state('app.agenda', {
        url: '/agenda',
        views: {
            'menuContent': {
                templateUrl: 'templates/agenda.html',
                controller: 'AgendaCtrl'
            }
        }
    })

    .state('app.agenda-dia', {
        url: '/agenda/:agendaDia',
        views: {
            'menuContent': {
                templateUrl: 'templates/agenda-dia.html',
                controller: 'AgendaDiaCtrl'
            }
        }
    })

    .state('app.noticias', {
        url: '/noticias',
        views: {
            'menuContent': {
                templateUrl: 'templates/noticias.html',
                controller: 'NoticiasCtrl'
            }
        }
    })

    .state('app.noticia', {
        url: '/noticias/:noticiaId',
        views: {
            'menuContent': {
                templateUrl: 'templates/noticia.html',
                controller: 'NoticiaCtrl'
            }
        }
    })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/home');
});
