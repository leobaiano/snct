<?php
	/**
	 * Plugin Name: Menus Hide
	 * Plugin URI:
	 * Description: Oculta itens do menu
	 * Author: leobaiano
	 * Author URI: http://lbideias.com.br
	 * Version: 0.0.1
	 * License: GPLv2 or later
	 * Text Domain: menus_hide
 	 * Domain Path: /languages/
	 */
	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly.

	/**
	 * Menus_Hide
	 *
	 * @author   Leo Baiano <leobaiano@lbideias.com.br>
	 */
	class Menus_Hide {
		/**
		 * Instance of this class.
		 *
		 * @var object
		 */
		protected static $instance = null;

		/**
		 * Prefix plugin
		 *
		 * @var string
		 */
		public static $prefix = 'menus_hide';


		/**
		 * Initialize the plugin
		 */
		private function __construct() {

			// Load plugin text domain
			add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );

			// Remove Menus
			add_action( 'admin_menu', array( $this, 'remove_menus' ), 1 );
		}

		/**
		 * Return an instance of this class.
		 *
		 * @return object A single instance of this class.
		 */
		public static function get_instance() {
			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 * Load the plugin text domain for translation.
		 *
		 * @return void
		 */
		public function load_plugin_textdomain() {
			load_plugin_textdomain( self::$prefix, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}

		function remove_menus () {
			// if (current_user_can( 'edit_themes' ) ) {
				global $menu;
				$restricted = array(
					__('Dashboard'),
					__('Posts'),
					__('Media'),
					__('Links'),
					__('Pages'),
					__('Appearance'),
					__('Tools'),
					// __('Users'),
					__('Settings'),
					__('Comments'),
					// __('Plugins')
				);
				end ($menu);
				while (prev($menu)){
					$value = explode(' ',$menu[key($menu)][0]);
					if( in_array($value[0] != NULL?$value[0]:"" , $restricted ) ) {
						unset( $menu[key($menu)] );
					}
				}
			// } else {
			// 	return false;
			// }
		}
	}
	add_action( 'plugins_loaded', array( 'Menus_Hide', 'get_instance' ), 0 );
