<?php
	/**
	 * Plugin Name: SNCT
	 * Plugin URI:
	 * Description: Plugin que cria os CPT's para o web service do APP SNCT
	 * Author: leobaiano
	 * Author URI: http://lbideias.com.br
	 * Version: 0.0.1
	 * License: GPLv2 or later
	 * Text Domain: snct
 	 * Domain Path: /languages/
	 */
	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly.

	require_once 'class-cpt.php';

	/**
	 * SNCT
	 *
	 * @author   Leo Baiano <leobaiano@lbideias.com.br>
	 */
	class SNCT {
		/**
		 * Instance of this class.
		 *
		 * @var object
		 */
		protected static $instance = null;

		/**
		 * Prefix plugin
		 *
		 * @var string
		 */
		public static $prefix = 'snct';


		/**
		 * Initialize the plugin
		 */
		private function __construct() {

			// Load plugin text domain
			add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );
			// Create Custom post types
			add_action( 'init', array( $this, 'create_cpts' ), 1 );
			add_action( 'init', array( $this, 'create_taxonomys' ), 1 );
		}
		/**
		 * Return an instance of this class.
		 *
		 * @return object A single instance of this class.
		 */
		public static function get_instance() {
			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}
			return self::$instance;
		}
		/**
		 * Load the plugin text domain for translation.
		 *
		 * @return void
		 */
		public function load_plugin_textdomain() {
			load_plugin_textdomain( self::$prefix, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Creates the post pin type that is used to add posts
		 *
		 */
		public function create_cpts(){
			new Create_CPT( 'schedule', 'Schedule', array( 'title', 'thumbnail' ), self::$prefix );
			new Create_CPT( 'wall', 'Pictures Wall', array( 'title', 'thumbnail' ), self::$prefix );
			new Create_CPT( 'news', 'News', array( 'title', 'editor', 'excerpt', 'thumbnail' ), self::$prefix );
			new Create_CPT( 'my-agenda', 'My Agenda', array( 'title', 'excerpt' ), self::$prefix );
			// new Create_CPT( 'location', 'Locations', array( 'title', 'editor', 'excerpt', 'thumbnail' ), self::$prefix );
		}

		/**
		 * Create Taxonomys
		 */
		public function create_taxonomys() {
			register_taxonomy('state', array( 'schedule', 'location' ), array( 'hierarchical' => true, 'label' => _x( 'State/ City', self::$prefix ), 'query_var' => true, 'rewrite' => true));
			// register_taxonomy('city', array( 'schedule', 'location' ), array( 'hierarchical' => true, 'label' => _x( 'City', self::$prefix ), 'query_var' => true, 'rewrite' => true));
		}
	}
	add_action( 'plugins_loaded', array( 'SNCT', 'get_instance' ), 0 );
