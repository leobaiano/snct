<?php
		header('Access-Control-Allow-Origin: *');
		if ($_FILES) {
			$nome = sanitize_text_field( $_POST['nome'] );
			$onde = sanitize_text_field( $_POST['onde'] );
			$comentario = sanitize_text_field( $_POST['comentario'] );

			$my_post = array(
				  	'post_title'    => $nome,
				  	'post_content'  => '',
				  	'post_status'   => 'publish',
				  	'post_author'   => 1,
				  	'post_type'		=> 'wall'
				);
			$post_id = wp_insert_post( $my_post );
			update_field( 'field_55ffbe1824b51', $onde, $post_id );
			update_field( 'field_55ffbe38df0e2', $comentario, $post_id );

			if ( ! function_exists( 'wp_handle_upload' ) ) {
	    		require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			$uploadedfile = $_FILES["file"];
			$upload_overrides = array( 'test_form' => false );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

			if ( $movefile && !isset( $movefile['error'] ) ) {
			    $caminho_absoluto = $movefile['file'];
			    $caminho_relativo = $movefile['url'];
			    $tipo_imagem = $movefile['type'];

			    $attachment = array(
	                'guid' => basename( $caminho_absoluto ),
	                'post_mime_type' => $tipo_imagem,
	                'post_title' => preg_replace('/\.[^.]+$/', '', basename( $caminho_absoluto ) ),
	                'post_content' => '',
	                'post_status' => 'publish'
	            );
	            $attach_id = wp_insert_attachment( $attachment, $caminho_absoluto, $post_id );

				require_once( ABSPATH . 'wp-admin/includes/image.php' );
	            $attach_data = wp_generate_attachment_metadata( $attach_id, $caminho_absoluto );
				wp_update_attachment_metadata( $attach_id, $attach_data );

				set_post_thumbnail( $post_id, $attach_id );
			}

			$my_post = array(
				'ID'           => $post_id,
				'post_status'   => 'pending',
			);
		  wp_update_post( $my_post );
		}



		// if ($_FILES) {

		// $dirname = "../friends/".trim($_POST['value1']);
		// $ran = $_POST['value2']."_".rand();
			// If uploading file
		// 		print_r($_FILES);

		// 	list($w, $h) = getimagesize($_FILES["file"]["tmp_name"]);
		// 	/* calculate new image size with ratio */
		// 	$width = 900;
		// 	$height = 900;
		// 	$ratio = max($width/$w, $height/$h);
		// 	$h = ceil($height / $ratio);
		// 	$x = ($w - $width / $ratio) / 2;
		// 	$w = ceil($width / $ratio);
		// 	/* new file name */
		// 	$path = trim($dirname)."/".$ran.".jpg";
		// 	/* read binary data from image file */
		// 	$imgString = file_get_contents($_FILES['file']['tmp_name']);
		// 	/* create image from string */
		// 	$image = imagecreatefromstring($imgString);
		// 	$tmp = imagecreatetruecolor($width, $height);
		// 	imagecopyresampled($tmp, $image,
		// 		0, 0,
		// 		$x, 0,
		// 		$width, $height,
		// 		$w, $h);
		// 	/* Save image */
		// 	switch ($_FILES['file']['type']) {
		// 		case 'image/jpeg':
		// 			imagejpeg($tmp, $path, 60);
		// 			break;
		// 		case 'image/png':
		// 			imagepng($tmp, $path, 0);
		// 			break;
		// 		case 'image/gif':
		// 			imagegif($tmp, $path);
		// 			break;
		// 		default:
		// 			exit;
		// 			break;
		// 	}
		// 	return $path;
		// 	echo $path;
		// 	/* cleanup memory */
		// 	imagedestroy($image);
		// 	imagedestroy($tmp);
		// }
