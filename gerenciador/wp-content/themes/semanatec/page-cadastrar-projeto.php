<?php
	Generic::security();
	get_header();
?>
	<div class="cabecalho-internas">
		<h2 class="text-center">Minha Rede</h2>
	</div>
	<section class="rede">
		<div class="container">
			<?php get_sidebar( 'rede' ); ?>
			<div class="content-interna col-lg-9">
				<div class="row">
					<form method="post" action="" class="cadastro cadastro-projeto" enctype="multipart/form-data">
						<input type="hidden" name="validacao" value="projeto">
						<h3 class="title-minha-rede"><?php the_title(); ?></h3>
						<div class="col-lg-12">
							<input type="text" name="titulo" placeholder="Titulo do projeto" required>
						</div>
						<div class="col-lg-12">
							<select name="c_categoria">
								<option value="">Categoria</option>
								<option value="artes">Artes</option>
								<option value="esporte">Esporte</option>
								<option value="musica">Música</option>
								<option value="teatro">Teatro</option>
							</select>
						</div>
						<div class="col-lg-12">
							<textarea name="descricao" placeholder="Descricao" required></textarea>
						</div>
						<div class="col-lg-5">
							<div class="row">
								<div class="col-lg-12">
									Imagem de destaque: <input type="file" name="imagem_destaque" placeholder="Upload de imagem de destaque" id="img_destaque_projeto" />
								</div>
								<div class="col-lg-12">
									<input type="text" name="url_video" placeholder="URL do Vídeo" />
								</div>
								<div class="col-lg-12">
									Projeto completo: <input type="file" name="arquivo_projeto" placeholder="Upload do projeto completo" required />
								</div>
							</div>
						</div>
						<div class="col-lg-7">
							<textarea name="equipe" placeholder="Equipe" style="height:215px;"></textarea>
						</div>

						<div class="col-lg-12">
							<h3 class="subtitulo-cadastro text-center">Etapas (Cronograma de Execução)</h3>
						</div>
						<div class="col-lg-12">
							<table class="table inserir-etapas">
								<thead>
									<tr>
										<th class="col-lg-1">Data Início</th>
										<th class="col-lg-1">Data Fim</th>
										<th class="col-lg-3">Nome</th>
										<th class="col-lg-5">Descrição</th>
										<th class="col-lg-2">Editar/ Excluir</th>
									</tr>
								</thead>
								<tbody class="corpo-etapa">
									<tr class="form-etapas">
										<td><input type="text" name="etapa_data_inicio" placeholder="dd/mm/aaaa" class="etapa_data_inicio"></td>
										<td><input type="text" name="etapa_data_fim" placeholder="dd/mm/aaaa" class="etapa_data_fim"></td>
										<td><input type="text" name="etapa_nome" placeholder="Nome" class="etapa_nome"></td>
										<td><input type="text" name="etapa_descricao" placeholder="Descrição" class="etapa_descricao"></td>
										<td>
											<a href="javascript:;" title="Inserir" class="bot-inserir-etapa"><i class="fa fa-check icon-inserir"></i> Inserir</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="col-lg-12">
							<div class="col-lg-12">
								<h3 class="subtitulo-cadastro text-center">Material</h3>
							</div>
							<div class="col-lg-12">
								<table class="table inserir-etapas">
									<thead>
										<tr>
											<th class="col-lg-2">Quantidade</th>
											<th class="col-lg-6">Nome</th>
											<th class="col-lg-2">Valor em Logos</th>
											<th class="col-lg-2">Editar/ Excluir</th>
										</tr>
									</thead>
									<tbody class="corpo-material">
										<tr class="form-etapas">
											<td><input type="number" min="1" name="material_quantidade" placeholder="0" class="material_qtd"></td>
											<td><input type="text" name="material_nome" placeholder="Nome" class="material_nome"></td>
											<td><input type="text" name="valor_em_logos" placeholder="ex.: 1000.00" class="valor_em_logos">
											<td>
												<a href="javascript:;" title="Inserir" class="bot-inserir-material"><i class="fa fa-check icon-inserir"></i> Inserir</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="col-lg-12">
								<h3 class="subtitulo-cadastro text-center">Colaboradores</h3>
							</div>
							<div class="col-lg-12">
								<table class="table inserir-etapas">
									<thead>
										<tr>
											<th class="col-lg-2">Quantidade</th>
											<th class="col-lg-7 col-lg-offset-1">Área de atuação</th>
											<th class="col-lg-2">Editar/ Excluir</th>
										</tr>
									</thead>
									<tbody class="corpo-colaborador">
										<tr class="form-etapas">
											<td><input type="number" min="1" name="colaborador_quantidade" placeholder="0" class="colaborador_qtd"></td>
											<td>
												<select name="area_colaboradores" class="colaborador_area">
													<option value="">Selecione a área</option>
													<?php
														$areas = Generic::get_areas();
														foreach ( $areas as $area ) {
													?>
															<option value="<?php echo $area->nome; ?>"><?php echo $area->nome; ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<a href="javascript:;" title="Inserir"class="bot-inserir-colaborador"><i class="fa fa-check icon-inserir"></i> Inserir</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="col-lg-4 col-lg-offset-4">
								<input type="submit" value="Cadastrar" class="btn-logos-rosa btn btn-primary">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
</body>
</html>
<?php
	if ( !empty( $_POST ) && isset( $_POST['validacao'] ) && 'projeto' == $_POST['validacao'] ) {
		$post_status = 'pending';
		$post_type = 'projeto';
		$post_author = get_current_user_id();
		$post_title = esc_attr( $_POST['titulo'] );
		$categoria = esc_attr( $_POST['c_categoria'] );
		$post_content = esc_textarea( $_POST['descricao'] );
		$imagem_destaque = $_POST['imagem_destaque'];
		$url_video = esc_attr( $_POST['url_video'] );
		$projeto = $_POST['arquivo_projeto'];
		$equipe = esc_textarea( $_POST['equipe'] );

		$defaults = array(
		            'post_status' => $post_status,
		            'post_type' => $post_type,
		            'post_author' => $post_author,
		            'post_title' => $post_title,
		            'post_content' => $post_content,
		        );
		$post_id = wp_insert_post( $defaults );
		wp_set_object_terms( $post_id, $categoria, 'categoria' );

		if ( ! function_exists( 'wp_handle_upload' ) ) {
    		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		$uploadedfile = $_FILES['imagem_destaque'];
		$upload_overrides = array( 'test_form' => false );
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
		if ( $movefile && !isset( $movefile['error'] ) ) {
		    $caminho_absoluto = $movefile['file'];
		    $caminho_relativo = $movefile['url'];
		    $tipo_imagem = $movefile['type'];

		    $attachment = array(
                'guid' => basename( $caminho_absoluto ),
                'post_mime_type' => $tipo_imagem,
                'post_title' => preg_replace('/\.[^.]+$/', '', basename( $caminho_absoluto ) ),
                'post_content' => '',
                'post_status' => 'publish'
            );
            $attach_id = wp_insert_attachment( $attachment, $caminho_absoluto, $post_id );

			require_once( ABSPATH . 'wp-admin/includes/image.php' );
            $attach_data = wp_generate_attachment_metadata( $attach_id, $caminho_absoluto );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			set_post_thumbnail( $post_id, $attach_id );
		}

		// Cadastrando as etapas
		if ( count( $_SESSION['projeto_etapas'] ) ) {
			$field_key = "field_55b29855e6066";
			$value = get_field( $field_key, $post_id );
			foreach ( $_SESSION['projeto_etapas'] as $row_etapa ) {
				$value[] = array(
								'etapa_data_inicio' => $row_etapa['etapa_data_inicio'],
								'etapa_data_fim' 	=> $row_etapa['etapa_data_fim'],
								'etapa_nome' 		=> $row_etapa['etapa_nome'],
								'etapa_descricao' 	=> $row_etapa['etapa_descricao'],
							);
			}
			update_field( $field_key, $value, $post_id );
		}

		// Cadastrando material
		if ( count( $_SESSION['projeto_material'] ) ) {
			$field_key_material = "field_55b29a9e75e8a";
			$value = get_field( $field_key_material, $post_id );
			foreach ( $_SESSION['projeto_material'] as $row_etapa ) {
				$value[] = array(
								'material_quantidade' => $row_etapa['material_quantidade'],
								'material_nome' 	=> $row_etapa['material_nome'],
								'valor_em_logos'	=> $row_etapa['valor_em_logos'],
							);
			}
			update_field( $field_key_material, $value, $post_id );
		}

		// Cadastrando colaborados
		if ( count( $_SESSION['projeto_colaborador'] ) ) {
			$field_key_colaborador = "field_55b29b1675e8d";
			$value = get_field( $field_key_colaborador, $post_id );
			foreach ( $_SESSION['projeto_colaborador'] as $row_etapa ) {
				$value[] = array(
								'colaborador_quantidade' => $row_etapa['colaborador_quantidade'],
								'colaborador_area' 	=> $row_etapa['colaborador_area'],
							);

				Generic::inserir_areas_projetos($row_etapa['colaborador_area'], $post_id );
			}
			update_field( $field_key_colaborador, $value, $post_id );
		}
		Generic::show_message( 'sucesso', 'Projeto cadastrado com sucesso.' );
	}
	unset( $_SESSION['projeto_etapas'] );
	unset( $_SESSION['projeto_material'] );
	unset( $_SESSION['projeto_colaborador'] );
