<?php
	@ini_set( 'upload_max_size' , '2M' );
	@ini_set( 'post_max_size', '64M');
	@ini_set( 'max_execution_time', '500' );

	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );

	add_filter('show_admin_bar', '__return_false');

	// Tamanhos personalizados de imagens
	if ( function_exists( 'add_theme_support' ) ) {
		add_image_size( "thumb", 200, 115, true);
		add_image_size( "medio", 400, 240, true);
		add_image_size( "destaque", 850, 350, true);
	}

	// CARREGA SCC E JSs
	function wp_load_scripts() {
		// Load Styles
		wp_enqueue_style( 'css-map', get_template_directory_uri() . '/assets/css/twetmap.css', array(), null, 'all' );
		wp_enqueue_style( 'css-bxslider', get_template_directory_uri() . '/assets/css/jquery.bxslider.css', array(), null, 'all' );
		wp_enqueue_style( 'css-style', get_template_directory_uri() . '/assets/css/estilo.css', array(), null, 'all' );


		// Load Scripts
		// jQuery.
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'bxslider-js', get_template_directory_uri() . '/assets/js/jquery.bxslider.min.js', array(), null, true );
		wp_enqueue_script( 'funcoes', get_template_directory_uri() . '/assets/js/main.js', array(), null, true );
	}
	add_action( 'wp_enqueue_scripts', 'wp_load_scripts', 1 );

function wp_api_encode_acf($data,$post,$context){
    $customMeta = (array) get_fields($post['ID']);
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
function wp_api_encode_acf_taxonomy($data,$post){
    $customMeta = (array) get_fields($post->taxonomy."_". $post->term_id );
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
function wp_api_encode_acf_user($data,$post){
    $customMeta = (array) get_fields("user_". $data['ID']);
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
add_filter('json_prepare_post', 'wp_api_encode_acf', 10, 3);
add_filter('json_prepare_page', 'wp_api_encode_acf', 10, 3);
add_filter('json_prepare_attachment', 'wp_api_encode_acf', 10, 3);
add_filter('json_prepare_term', 'wp_api_encode_acf_taxonomy', 10, 2);
add_filter('json_prepare_user', 'wp_api_encode_acf_user', 10, 2);

function public_post_in( $qvars ) {
	$qvars[] = 'post__in';
	return $qvars;
}
add_filter('query_vars', 'public_post_in', 10, 2);

function wp_api_encode_term_state($data,$post,$context) {
	$data['state'] = array();
	$terms = wp_get_post_terms( $post['ID'], 'state' );
	foreach($terms as $term) {
		$array = array(
					'id' => $term->term_id,
					'name' => $term->name,
					'parent' => $term->parent
				);
		$data['meta'] = array_merge($data['state'], $array );
	}
	return $data;
}
// add_filter('json_prepare_post', 'wp_api_encode_term_state', 10, 3);

	include("assets/cpt/destaque.php");
	include("assets/cpt/fotos.php");
	// include("assets/cpt/releases.php");
	include("assets/cpt/videos.php");
	include("assets/cpt/jogos.php");




	if ( !empty( $_GET['importar'] ) ) {
		function do_my_action(){
			require_once '1.7.6/Classes/PHPExcel.php';

			$objReader = new PHPExcel_Reader_Excel5();
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load( 'eventos.xls' );
			$objPHPExcel->setActiveSheetIndex(0);

			for( $linha = 2; $linha <= 130; $linha++ ){
				for( $coluna = 0; $coluna <= 9; $coluna++ ){
					if( $coluna == 0 )
						$estado = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
					if( $coluna == 1 )
						$cidade = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
					if( $coluna == 2 )
						$titulo = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
					if( $coluna == 3 )
						$local = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
					if( $coluna == 4 )
						$data = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
					if( $coluna == 6 )
						$endereco = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
					if( $coluna == 8 )
						$latitude = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
					if( $coluna == 9 )
						$longitude = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
				}
				$data = str_replace('&', '/', $data);
				$estado = strtolower( $estado );
				$cidade = strtolower( $cidade );

				$my_post = array(
				  	'post_title'    => $titulo,
				  	'post_content'  => '',
				  	'post_status'   => 'publish',
				  	'post_author'   => 1,
				  	'post_type'		=> 'schedule'
				);

				$post_id = wp_insert_post( $my_post );

				wp_set_object_terms( $post_id, $estado, 'state', true );
				wp_set_object_terms( $post_id, $cidade, 'state', true );

				update_field( 'field_55fb1ec64d80e', $local, $post_id );
				update_field( 'field_55fb1ed24d80f', $data, $post_id );
				update_field( 'field_55fb1ef74d811', $endereco, $post_id );
				update_field( 'field_55fb1f3f4364a', $latitude, $post_id );
				update_field( 'field_55fb1f4c4364b', $longitude, $post_id );
			}
		}

		// add_action('init', 'do_my_action', 99);
	}
?>
