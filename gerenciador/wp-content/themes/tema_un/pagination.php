<?php
/**
 * The pagination for our theme.
 *
 * Pagination
 *
 * @package Tema_UN
 */
$args = array( 	'big_number' => 999999999,
    'base'       => str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
    'format'     => '?paged=%#%',
    'current'    => max( 1, get_query_var( 'paged' ) ),
    'total'      => $wp_query->max_num_pages,
    'prev_next'  => true,
    'end_size'   => 1,
    'mid_size'   => 4,
    'type'       => 'list'
);
echo '<div class="box_paginacao">';
	echo paginate_links( $args );
echo '</div>';
