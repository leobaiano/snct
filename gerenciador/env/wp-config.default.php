<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 *
 * Please note if you add constants in this file (i.e. define statements)
 * these cannot be overridden in environment config files.
 *
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+g5mAV`H@IcsZb-b#4}|NYO4]lRhK7GcMbi87?i4@H5}va8zCn7QPZ~1Vu)QKkiV');
define('SECURE_AUTH_KEY',  'Xab-bu%Y*PA-x~ea,B&CmL{/4I0r&-*G!0v bE04//6zKmki#$aB?Mo{M0?^(hlI');
define('LOGGED_IN_KEY',    ']bb5[),s+ouG`0QI|-`jYroO>%/NS||niOrx& ;Igm$Ye.[YJrtPLEExM6?4(uwu');
define('NONCE_KEY',        '89wSL]q]Mighf8+D+ >4-0#B=%o4~L`G]Pj-f+QN~D +u.I[;(M]^VaFv!=K#WG~');
define('AUTH_SALT',        'd@1dtb-:|MxhDkv[cz|&Rjlu$|u+&hO~|$Y|(__Ef~S_bp,ZV>/vKTJvpwo5n`91');
define('SECURE_AUTH_SALT', '4T6-|P;Vtz>/=w`Y4u2)-&y=z}#=XG_I7$62<}9?Hoa^}=U/xE^5K/fOuTH%f6|X');
define('LOGGED_IN_SALT',   'O|~on|m.{Ne+^U8eml>=>|dyU+!:8^yJP@|h/.D_If?2G@3q19#(Ln4FLT&Q08[Q');
define('NONCE_SALT',       'x-q+zl62aSrcrX FiLU@z!ZP[6nH!Cx[6y=(>5{S`i^~5z9V2p,p6>kLgo)Cq&fO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'pt_BR');
